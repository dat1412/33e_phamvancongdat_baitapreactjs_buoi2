import React, { Component } from "react";
import { data } from "./dataGlasses";

export default class Content extends Component {
  state = {
    glasses: data,
    vGlasses: data[0],
  };

  // Render Mẫu
  renderModel = () => {
    return (
      <>
        <div className="glasessModel bg-[url('./glassesImage/model.jpg')] bg-center bg-cover h-96 w-80 mt-6">
          <img
            className="w-40 absolute top-24 left-20 opacity-80"
            src={this.state.vGlasses.url}
            alt="glassesModel"
          />
        </div>
        <div className="glassesInfo text-left p-2 bg-orange-300 bg-opacity-50 absolute right-0 bottom-0">
          <h3 className="text-blue-500 font-semibold mb-1 text-lg">
            {this.state.vGlasses.name}
          </h3>
          <span className="text-white bg-red-600 p-1 rounded text-sm">
            {this.state.vGlasses.price}$
          </span>
          <p className="text-white font-medium mt-1 text-xs">
            {this.state.vGlasses.desc}
          </p>
        </div>
      </>
    );
  };

  // Đổi kính
  handleChangeGlasses = (item) => {
    this.setState({
      vGlasses: item,
    });
  };

  // Render danh sách kính
  renderGlassesLisst = () => {
    return this.state.glasses.map((item) => {
      return (
        <button
          onClick={() => {
            this.handleChangeGlasses(item);
          }}
          className="w-48 p-4"
        >
          <img src={item.url} alt="glasses" />
        </button>
      );
    });
  };
  render() {
    return (
      <div className="content m-auto w-3/4">
        <div className="model relative m-auto h-96 w-80">
          {this.renderModel()}
        </div>
        <div className="glassesList bg-white text-left mt-8">
          {this.renderGlassesLisst()}
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import Content from "./Content";
import Header from "./Header";

export default class BaiTapState extends Component {
  render() {
    return (
      <div className="h-screen bg-no-repeat bg-top bg-cover bg-[rgba(0,0,0,0.5)] bg-[url('./glassesImage/background.jpg')]">
        <Header />
        <Content />
      </div>
    );
  }
}
